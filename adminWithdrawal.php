<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$withdrawalDetails = getWithdrawal($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminWithdrawal.php" />
    <meta property="og:title" content="Admin Withdrawal | De Xin Guo Ji 德鑫国际" />
    <title>Admin Withdrawal | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminWithdrawal.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <h1 class="menu-distance h1-title white-text text-center" ><span class="blue-link"><?php echo _ADMINDASHBOARD_WITHDRAW_REQUEST ?></span> | <a href="adminWithdrawalComp.php" ><?php echo _ADMINWITHDRAWAL_COMPLETE ?></a> | <a href="adminWithdrawalReject.php"><?php echo _ADMINWITHDRAWAL_REJECT ?></a> </h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">
        <table class="table-width data-table message-table">
                        <thead>
                            <tr>
                                <th><?php echo _VIEWMESSAGE_NO ?></th>
                                <th class="two-white-border"><?php echo _ADMINWITHDRAWAL_USERNAME ?></th>
                                <th class="two-white-border"><?php echo _ADMINWITHDRAWAL_AMOUNT ?></th>
                                <th class="two-white-border"><?php echo _ADMINWITHDRAWAL_REQUEST_TIME ?></th>
                                <th><?php echo _ADMINWITHDRAWAL_VERIFY ?></th>
                            </tr>
                        </thead>
    
                        <tbody>
                            <?php
    
                            if($withdrawalDetails != null)
                            {   
                                for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                                {?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $withdrawalDetails[$cnt]->getUsername();?></td>
                                    <td><?php echo $withdrawalDetails[$cnt]->getAmount();?></td>
    
                                    <td>
                                        <?php $dateCreated = date("Y-m-d",strtotime($withdrawalDetails[$cnt]->getDateCreated())); echo $dateCreated;?>
                                    </td>
    
                                    <td>
                                        <!-- <form action="adminWithdrawalVerification.php" method="POST"> -->
                                        <form action="adminVerifyWithdrawal.php" method="POST">
                                            <button class="clean hover1 blue-button smaller-font" type="submit" name="withdrawal_id" value="<?php echo $withdrawalDetails[$cnt]->getId();?>">
                                                <?php echo _ADMINWITHDRAWAL_VERIFY ?>
                                            </button>
                                        </form>
                                    </td>
    
                                <?php
                                }?>
                                </tr>
                            <?php
                            }
    
                            ?>
                        </tbody>
    
                    </table>
            </div>
	</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>