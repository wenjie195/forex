<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$totalProfit = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('LOSE'), "s");
$totalLose = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('WIN'), "s");
$withdrawalRequest = getWithdrawal($conn," WHERE status = ? ",array("status"),array('PENDING'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | De Xin Guo Ji 德鑫国际" />
    <title>Admin Dashboard | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminDashboard.php" />

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
	<div class="same-padding2 menu-distance">
		<h1 class="h1-title white-text text-center"><?php echo _SIDEBAR_DASHBOARD ?></h1>    
    	<div class="width100 overflow">
        	<a>
                <div class="five-div blue-border-hover">
                    <img src="img/win.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_NO_OF_WIN ?>" title="<?php echo _ADMINDASHBOARD_NO_OF_WIN ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_NO_OF_WIN ?></p> 
                        <?php
                        if($totalProfit)
                        {   
                            $totalProfitNumCount = count($totalProfit);
                        }
                        else
                        {   $totalProfitNumCount = 0;   }
                        ?>
                    <p class="blue-border-p"><?php echo $totalProfitNumCount;?></p> 
                </div>
            </a>

            <a href="adminReportWin.php" class="overflow">
                <div class="five-div blue-border-hover left-five-div">
                    <img src="img/total-profit.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_TOTAL_WIN ?>" title="<?php echo _ADMINDASHBOARD_TOTAL_WIN ?>">
                        <p class="blue-border-p"><?php echo _ADMINDASHBOARD_TOTAL_WIN ?></p> 
                            <?php
                            if($totalProfit)
                            {
                                $totalProfitAmount = 0;
                                for ($cnt=0; $cnt <count($totalProfit) ; $cnt++)
                                {
                                $totalProfitAmount += $totalProfit[$cnt]->getAmount();
                                }
                            }
                            else
                            {
                                $totalProfitAmount = 0 ;
                            }
                            ?>
                        <p class="blue-border-p"><?php echo $totalProfitAmount;?></p> 
                </div>
            </a>
        	<a>
                <div class="five-div blue-border-hover">
                    <img src="img/loss.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_NO_OF_LOSS ?>" title="<?php echo _ADMINDASHBOARD_NO_OF_LOSS ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_NO_OF_LOSS ?></p> 
                        <?php
                        if($totalLose)
                        {   
                            $totalLoseNumCount = count($totalLose);
                        }
                        else
                        {   $totalLoseNumCount = 0;   }
                        ?>
                        <p class="blue-border-p"><?php echo $totalLoseNumCount;?></p> 
                </div>
            </a> 
            <a href="adminReportLose.php" class="overflow">
                <div class="five-div blue-border-hover right-five-div">
                    <img src="img/total-loss.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_TOTAL_LOSS ?>" title="<?php echo _ADMINDASHBOARD_TOTAL_LOSS ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_TOTAL_LOSS ?></p> 
                            <?php
                            if($totalLose)
                            {
                                $totalLoseAmount = 0;
                                for ($cnt=0; $cnt <count($totalLose) ; $cnt++)
                                {
                                $totalLoseAmount += $totalLose[$cnt]->getAmount();
                                }
                            }
                            else
                            {
                                $totalLoseAmount = 0 ;
                            }
                            ?>
                    <p class="blue-border-p"><?php echo $totalLoseAmount;?></p> 
                </div>
            </a>             
            <a href="adminWithdrawal.php" class="overflow">
                <div class="five-div blue-border-hover">
                    <img src="img/withdraw-request.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_WITHDRAW_REQUEST ?>" title="<?php echo _ADMINDASHBOARD_WITHDRAW_REQUEST ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_WITHDRAW_REQUEST ?></p> 
                        <?php
                        if($withdrawalRequest)
                        {   
                            $totalWithdrawalRequest = count($withdrawalRequest);
                        }
                        else
                        {   $totalWithdrawalRequest = 0;   }
                        ?>
                    <p class="blue-border-p"><?php echo $totalWithdrawalRequest;?></p> 
                </div>
            </a>                                    
        </div>
    </div>





</div>
<style>
.dashboard-li .hover1b{
	display:inline-block;
	}
.dashboard-li .hover1a{
	display:none;
	}
.dashboard-li .sidebar-span{
    color: #94C6F2;}
.dashboard-li{
	background-color:#15212d;}
</style>

<?php include 'js.php'; ?>

</body>
</html>