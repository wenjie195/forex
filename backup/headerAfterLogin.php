<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/logo.png" class="logo-img" alt="<?php echo _HEADER_DEXINGUOJI ?>" title="<?php echo _HEADER_DEXINGUOJI ?>">
            </div>
            <div class="right-menu-div float-right">
                <div class="dropdown hover1 menu-item menu-a">
                	<?php echo _HEADER_LANGUAGE ?>	
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                	
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en" class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch" class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
				</div>
                	<!-- Delete this <a> when you all ready to do the edit profile/password page-->
                	<!-- <a href="userDashboard.php" class="menu-item menu-a menu-padding"><?php echo _HEADER_PROFILE ?></a> -->
 					<!-- Unhide this <div> when you all ready to do the edit profile/password page-->
                    <!-- <a href="viewMessage.php" class="menu-item menu-a menu-padding"><div><?php //echo _HEADER_MESSAGE ?><div class="menu-red-dot"></div></div></a> -->





                    <?php
                    require_once dirname(__FILE__) . '/classes/Message.php';
                    $conn = connDB();
                    $messageValue = getMessage($conn," WHERE user_status = ? ",array("user_status"),array('GET'),"s");
                    if($messageValue)
                    {   
                    $totalMessageValue = count($messageValue);
                    // echo $totalMessageValue;
                        if($totalMessageValue > 0)
                        {   
                        ?>
                        <a href="viewMessage.php" class="menu-item menu-a menu-padding"><div><?php echo _HEADER_MESSAGE ?><div class="menu-red-dot"></div></div></a>
                        <?php
                        }
                        else
                        {   
                        ?>
                        <a href="viewMessage.php" class="menu-item menu-a menu-padding"><div><?php echo _HEADER_MESSAGE ?></div></a>
                        <?php
                        }
                    }
                    else
                    {   
                        $totalMessageValue = 0;   
                        ?>
                        <a href="viewMessage.php" class="menu-item menu-a menu-padding"><div><?php echo _HEADER_MESSAGE ?></div></a>
                        <?php
                    }
                    ?>


                     <a href="#" class="menu-item menu-a menu-padding"></a>
                    <div class="dropdown hover1 menu-item menu-a">
                        <?php echo _HEADER_PROFILE ?>	
                                    <img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADER_PROFILE ?>" title="<?php echo _HEADER_PROFILE ?>">
                                    <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADER_PROFILE ?>" title="<?php echo _HEADER_PROFILE ?>">
                        
                        <div class="dropdown-content yellow-dropdown-content">
                        			<p class="dropdown-p"><a href="userDashboard.php" class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _HEADER_PROFILE ?></a></p>
                                    <p class="dropdown-p"><a href="editProfile.php" class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _HEADER_EDIT_PROFILE ?></a></p>
                                    <!-- <p class="dropdown-p"><a href="editEmail.php" class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _HEADER_CHANGE_EMAIL ?></a></p> -->
                                    <!-- <p class="dropdown-p"><a href="editContact.php" class="menu-padding dropdown-a black-menu-item menu-a"><?php //echo _HEADER_CHANGE_PHONE_NO ?></a></p> -->
                                    <p class="dropdown-p"><a href="editPassword.php" class="menu-padding dropdown-a black-menu-item menu-a"><?php echo _HEADER_CHANGE_PASSWORD ?></a></p>
                        </div>
                    </div>                    
                    
                   
                    
                    <!-- End of Hiding -->
                <a href="logout.php" class="menu-padding menu-item menu-a"><?php echo _HEADER_LOGOUT ?></a>
					
                    
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="<?php $link ?>?lang=en">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">中文</a></li>
                                  <li><a href="viewMessage.php"><?php echo _HEADER_MESSAGE ?></a></li>
                                  <!-- <li><a href="#"><div class="menu-red-dot"></div></a></li> -->
                                  <li><a href="userDashboard.php"><?php echo _HEADER_PROFILE ?></a></li>
                                  <li><a href="editProfile.php" ><?php echo _HEADER_EDIT_PROFILE ?></a></li>
                                  <li><a href="editPassword.php" ><?php echo _HEADER_CHANGE_PASSWORD ?></a></li>  
                                  <li><a href="logout.php"><?php echo _HEADER_LOGOUT ?></a></li>                                                                                                    
   
						</ul>
					</div><!-- /dl-menuwrapper -->                
                
                                       	
            </div>
        </div>

</header>
