<?php
class Withdrawal {
    /* Member variables */
    // var $id, $uid, $username, $contact, $bankName, $amount, $currentCredit, $charges, $status, 
    //         $reference, $approvedBy, $approvedDateTime, $dateCreated, $dateUpdated;
    var $id, $uid, $username, $contact, $bankName, $bankAccNumber, $amount, $currentCredit, $charges, $status, 
    $reference, $approvedBy, $approvedDateTime, $dateCreated, $dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
        
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccNumber()
    {
        return $this->bankAccNumber;
    }

    /**
     * @param mixed $bankAccNumber
     */
    public function setBankAccNumber($bankAccNumber)
    {
        $this->bankAccNumber = $bankAccNumber;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrentCredit()
    {
        return $this->currentCredit;
    }

    /**
     * @param mixed $currentCredit
     */
    public function setCurrentCredit($currentCredit)
    {
        $this->currentCredit = $currentCredit;
    }

    /**
     * @return mixed
     */
    public function getCharges()
    {
        return $this->charges;
    }

    /**
     * @param mixed $charges
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param mixed $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return mixed
     */
    public function getApprovedDateTime()
    {
        return $this->approvedDateTime;
    }

    /**
     * @param mixed $approvedDateTime
     */
    public function setApprovedDateTime($approvedDateTime)
    {
        $this->approvedDateTime = $approvedDateTime;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getWithdrawal($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","contact","bank_name","bank_acc_number","amount","current_credit","charges","status",
                            "reference","approved_by","approved_datetime","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"withdrawal");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $uid, $username, $contact, $bankName, $bankAccNumber, $amount, $currentCredit, $charges, $status, 
                                $reference, $approvedBy, $approvedDateTime, $dateCreated, $dateUpdated);
    
        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new Withdrawal;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setContact($contact);
            $user->setBankName($bankName);

            $user->setBankAccNumber($bankAccNumber);

            $user->setAmount($amount);
            $user->setCurrentCredit($currentCredit);
            $user->setCharges($charges);
            $user->setStatus($status);
            $user->setReference($reference);
            $user->setApprovedBy($approvedBy);
            $user->setApprovedDateTime($approvedDateTime);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
