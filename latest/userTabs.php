<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
// set API Endpoint and API key
// $endpoint = 'latest';
// $access_key = 'ee34cf5a0d55806abbd902c8745af340';

// Initialize CURL:
// $ch = curl_init('http://data.fixer.io/api/'.$endpoint.'?access_key='.$access_key.'');
// $ch = curl_init('https://api.polygon.io/v1/last_quote/currencies/AUD/EUR?apiKey=CIS_tP7aDeaI0NQZTKnebo4J_NH7ld57R1ZMs4');
// $ch = curl_init('https://financialmodelingprep.com/api/v3/forex');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
// // Store the data:
// $json = curl_exec($ch);
// curl_close($ch);
//
// // Decode JSON response:
// $exchangeRates = json_decode($json, true);
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$exchangeRates = json_decode($response, true);
}

// print_r($exchangeRates['forexList']);
// Access the exchange rate values, e.g. GBP:

?>
		<table class="table-width data-table">
        	<thead>
            	<tr>
                	<th>
                    	<form>
                    		<input class="white-border-input search-font" placeholder="<?php echo _USERDASHBOARD_SEARCH ?>" type="text">
                        	<button class="clean search-btn hover1"><img src="img/search.png" class="search-img hover1a" alt="<?php echo _USERDASHBOARD_SEARCH ?>" title="<?php echo _USERDASHBOARD_SEARCH ?>"><img src="img/search2.png" class="search-img hover1b" alt="<?php echo _USERDASHBOARD_SEARCH ?>" title="<?php echo _USERDASHBOARD_SEARCH ?>"></button>
                        </form>
                    </th>
                    <th><?php echo _USERDASHBOARD_BUY ?></th>
                    <th><?php echo _USERDASHBOARD_SELL ?></th>
                    <th><?php echo _USERDASHBOARD_CHANGE ?></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
				<?php
				if ($exchangeRates)
				{
					$currencyArray = ("EURUSD,USDEUR,USDCAD,GBPJPY,AUDUSD,USDAUD,JPYUSD,USDJPY,GBPUSD,USDCHF");
					$currencyArrayWithSlash = ("EUR/USD,USD/EUR,USD/CAD,GBP/JPY,AUD/USD,USD/AUD,JPY/USD,USD/JPY,GBP/USD,USD/CHF");
					$currencyArrayExplode = explode(",",$currencyArray);
					$currencyArrayWithSlashExplode = explode(",",$currencyArrayWithSlash);
					?><input type="hidden" id="total" value="<?php echo count($currencyArrayExplode); ?>"> <?php
					for ($cntAA=0; $cntAA <count($currencyArrayExplode) ; $cntAA++)
					{
						for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
						{
							$selectCountry = str_replace("C:","",$exchangeRates['forexList'][$cnt]['ticker']);
							if ($selectCountry == $currencyArrayWithSlashExplode[$cntAA])
							{
								?>
								<tr class="open-buy">
									<td ><?php  echo $currencyArrayWithSlashExplode[$cntAA] ; ?></td>
									<td><?php echo number_format($exchangeRates['forexList'][$cnt]['bid'], 4); ?></td>
									<td><?php   echo number_format($exchangeRates['forexList'][$cnt]['ask'], 4); ?></td>
									<?php if ($exchangeRates['forexList'][$cnt]['changes'] < 0)
									{
									?>
										<td class="red-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
									<?php
									}
									elseif ($exchangeRates['forexList'][$cnt]['changes'] >= 0)
									{
									?>
										<td class="green-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
									<?php
									}
									?>

<td><button id="<?php echo "currency_nameSell".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>" class="clean transparent-btn blue-button open-buy red-btn table-btn-font"><?php echo _USERDASHBOARD_SELL ?></button></td>
<td><button id="<?php echo "currency_name".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>"  class="clean transparent-btn blue-button open-sell green-btn table-btn-font" ><?php echo _USERDASHBOARD_BUY ?></button></td>

								</tr>
								<?php
							}
						}
					}
				}
				?>
            </tbody>
		</table>
		
				<script type="text/javascript">
				var total = $("#total").val();
				for (var i = 0; i < total; i++) {
					$('#currency_name'+i+'').click( function(){
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>BUY</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});

					$('#currency_nameSell'+i+'').click( function(){
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>SELL</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});
				}
				</script>
