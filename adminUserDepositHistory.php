<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Deposit.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminUserDepositHistory.php" />
    <meta property="og:title" content="Deposit History | De Xin Guo Ji 德鑫国际" />
    <title>Deposit History | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminUserDepositHistory.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<div class="dark-bg overflow same-padding">
    <?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <h1 class="menu-distance h1-title white-text text-center">Deposit History</h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">

            <div class="overflow-scroll-div">
                <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th><?php echo _VIEWMESSAGE_NO ?></th>
                            <th>Amount (RM)</th>
                            <th>Top Up By</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $conn = connDB();
                        $depositArray = getDeposit($conn,"WHERE username = ? ", array("username") ,array($_POST['user_username']),"s");
                        if($depositArray != null)
                        {   
                            for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $depositArray[$cnt]->getAmount();?></td>
                                <td><?php echo $depositArray[$cnt]->getVerifyBy();?></td>
                                <td><?php echo $depositArray[$cnt]->getDateCreated();?></td>
                            <?php
                            }
                            ?>
                            </tr>
                        <?php
                        }
                        ?>        
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>