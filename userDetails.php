<div class="width100 menu-distance">
    <div class="width100 overflow profile-details-div">
    	<div class="user-left-div">
        	<div class="profile-div">
            	<img src="img/profile.jpg" class="profile-pic2 width100" alt="<?php echo _USERDASHBOARD_PROFILE ?>" title="<?php echo _USERDASHBOARD_PROFILE ?>">
            </div>
            <div class="profile-info-div">
            	<p class="profile-p">
                	<?php echo _USERDASHBOARD_ACC_NO ?> : <?php echo date('mdhi', strtotime($userDetails->getDateCreated()));?><?php echo $userDetails->getId();?>
                </p>
                <p class="profile-p"><?php echo _USERDASHBOARD_BALANCE ?>: 5000</p>
            </div>
        </div>
    	<div class="user-right-div">
        	<img src="img/logo3.png" alt="De Xin Guo Ji 德鑫国际" title="De Xin Guo Ji 德鑫国际" class="logo2-img web-logo1">
            <img src="img/logo2.png" alt="De Xin Guo Ji 德鑫国际" title="De Xin Guo Ji 德鑫国际" class="logo2-img mobile-logo1">
        </div>
    </div>
    <div class="clear"></div>
    <div class="width100 extra-m-btm small-distance x-distance">
    	<div class="blue-button open-withdraw text-center w30 float-left profile-3-btn ow-green-bg"><?php echo _USERDASHBOARD_WITHDRAWAL ?></div>

        <?php
            if($messageValue)
            {   
                $aaaa = count($messageValue);
                ?>
                    <!-- Red dot here -->
                    <!-- <div class="red-dot"><p class="red-dot-p"><?php //echo $aaaa;?></p></div> -->
                    <!-- End of Red Dot -->

                    <a href="viewMessage.php" class="inline-block w30 mid-30 float-left profile-3-btn">
                        <div class="blue-button text-center width100 follow-p3-font">
                            <?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?><!--(<?php //echo $aaaa;?>)-->
                        </div>
                        <div class="red-dot"></div>
                    </a>

                <?php
            }
            else
            {   
                $aaaa = 0;  
                ?>
                    <a href="viewMessage.php" class="inline-block w30 mid-30 float-left profile-3-btn">
                        <div class="blue-button text-center width100 follow-p3-font">
                            <?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?>
                        </div>
                    </a>
                <?php
            }
        ?>  
		<button class="blue-button open-withdraw text-center w30 float-left clean profile-3-btn ow-gold-bg"><?php echo _ADMINMEMBER_ADDCREDIT ?></button>
    </div>
</div>

<div class="clear"></div>