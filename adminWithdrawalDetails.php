<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$withdrawalDetails = getWithdrawal($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminWithdrawalDetails.php" />
    <meta property="og:title" content="Withdrawal | De Xin Guo Ji 德鑫国际" />
    <title>Withdrawal | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminWithdrawalDetails.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center">Withdrawal Details</h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">

        <h1 class="details-h1" onclick="goBack()">
            <a class="black-white-link2 hover1">
                Withdrawal Number : #<?php echo $_POST['withdrawal_id'];?>
            </a>
        </h1>

            <div class="width100 shipping-div2">
                <table class="details-table">
                    <tbody>
                    <?php
                    if(isset($_POST['withdrawal_id']))
                    {
                        $conn = connDB();
                        //Order
                        $depositArray = getWithdrawal($conn,"WHERE id = ? ", array("id") ,array($_POST['withdrawal_id']),"i");

                        if($depositArray != null)
                        {
                            ?>
                            <tr>
                                <td>Username</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getUsername()?></td>
                            </tr>
                            <tr>
                                <td>Bank Name</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getBankName()?></td>
                            </tr>
                            <tr>
                                <td>Withdrawal Amount</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getAmount()?></td>
                            </tr>
                            <tr>
                                <td>Request Date</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getDateCreated();?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getStatus();?></td>
                            </tr>
                            <tr>
                                <td>Process Date</td>
                                <td>:</td>
                                <td><?php echo $depositArray[0]->getDateUpdated();?></td>
                            </tr>

                        <?php
                        }
                    }?>
                    </tbody>
                </table>
            </div>
    </div>
	</div>
</div>
<?php include 'js.php'; ?>

</body>
</html>