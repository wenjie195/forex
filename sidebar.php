<div id="wrapper" class="toggled-2">

<div id="sidebar-wrapper">
<ul class="sidebar-nav nav-pills nav-stacked" id="menu">
    <li class="sidebar-li hover1 dashboard-li">
        <a href="adminDashboard.php" class="overflow">
        	<img src="img/dashboard.png" alt="<?php echo _SIDEBAR_DASHBOARD ?>" title="<?php echo _SIDEBAR_DASHBOARD ?>" class="sidebar-icon-img hover1a">
            <img src="img/dashboard2.png" alt="<?php echo _SIDEBAR_DASHBOARD ?>" title="<?php echo _SIDEBAR_DASHBOARD ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_DASHBOARD ?></p>
        </a>
    </li>
    <li class="sidebar-li hover1 acc-li">
        <a href="accountCreation.php" class="overflow">
        	<img src="img/account-creation.png" alt="<?php echo _SIDEBAR_ACC_CREATION ?>" title="<?php echo _SIDEBAR_ACC_CREATION ?>" class="sidebar-icon-img hover1a">
            <img src="img/account-creation2.png" alt="<?php echo _SIDEBAR_ACC_CREATION ?>" title="<?php echo _SIDEBAR_ACC_CREATION ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_ACC_CREATION ?></p>
        </a>
    </li>
    <li class="sidebar-li hover1 customer-li">
        <a href="" class="overflow">
        <a href="adminMemberList.php" class="overflow">
        	<img src="img/customer-list.png" alt="<?php echo _SIDEBAR_CUSTOMER_LIST ?>" title="<?php echo _SIDEBAR_CUSTOMER_LIST ?>" class="sidebar-icon-img hover1a">
            <img src="img/customer-list2.png" alt="<?php echo _SIDEBAR_CUSTOMER_LIST ?>" title="<?php echo _SIDEBAR_CUSTOMER_LIST ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_CUSTOMER_LIST ?></p>
        </a>
    </li>    
    <li class="sidebar-li hover1 withdraw-li">
        <!-- <a href="" class="overflow"> -->
        <a href="adminWithdrawal.php" class="overflow">
        	<img src="img/withdraw.png" alt="<?php echo _SIDEBAR_WITHDRAW_REQUEST ?>" title="<?php echo _SIDEBAR_WITHDRAW_REQUEST ?>" class="sidebar-icon-img hover1a">
            <img src="img/withdraw2.png" alt="<?php echo _SIDEBAR_WITHDRAW_REQUEST ?>" title="<?php echo _SIDEBAR_WITHDRAW_REQUEST ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_WITHDRAW_REQUEST ?></p>
        </a>
    </li>     
    <li class="sidebar-li hover1 trade-li">
        <a href="" class="overflow">
        <a href="adminCurrentTrade.php" class="overflow">
        	<img src="img/trade.png" alt="<?php echo _SIDEBAR_CURRENT_TRADE ?>" title="<?php echo _SIDEBAR_CURRENT_TRADE ?>" class="sidebar-icon-img hover1a">
            <img src="img/trade2.png" alt="<?php echo _SIDEBAR_CURRENT_TRADE ?>" title="<?php echo _SIDEBAR_CURRENT_TRADE ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_CURRENT_TRADE ?></p>
        </a>
    </li>  
    <li class="sidebar-li hover1 profit-li">
        <a href="" class="overflow">
        <!-- <a href="adminReportWin.php" class="overflow"> -->
        <a href="adminTotalProfitLoss.php" class="overflow">
        	<img src="img/analysis1.png" alt="<?php echo _SIDEBAR_TOTAL_PROFIT ?>" title="<?php echo _SIDEBAR_TOTAL_PROFIT ?>" class="sidebar-icon-img hover1a">
            <img src="img/analysis2.png" alt="<?php echo _SIDEBAR_TOTAL_PROFIT ?>" title="<?php echo _SIDEBAR_TOTAL_PROFIT ?>" class="sidebar-icon-img hover1b">
            <p class="sidebar-span"><?php echo _SIDEBAR_TOTAL_PROFIT ?></p>
        </a>
    </li>      
</ul>
</div>



</div>



<!--
<div class="menu-distance sidebar1">
	<div class="sidebar-icon-div">
		<a><img src="img/dashboard.png" alt="" title=""></a>
    </div>
    <div class="sidebar-text-div">
    	<a>Dashboard</a>
    </div>



</div>-->