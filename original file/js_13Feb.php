<div class="width100 same-padding footer-div">
	<p class="footer-p white-text"><?php echo _JS_FOOTER ?></p>
</div>

<!-- Login Modal -->
<div id="login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-login">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _JS_LOGIN ?></h1>
         <form action="utilities/loginFunction.php" method="POST">
         	<input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="username" name="username" required>
         	<input class="clean de-input" type="password" placeholder="<?php echo _JS_PASSWORD ?>Password" id="password" name="password" required>
            <input class="clean" type="checkbox"><label class="white-text"><?php echo _JS_REMEMBER_ME ?></label>
            <div class="clear"></div>
            <button class="clean blue-button mid-button-width small-distance small-distance-bottom" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <div class="clear"></div>
            <a class="open-forgot white-a forgot-a"><?php echo _JS_FORGOT_PASSWORD ?></a>
            <div class="clear"></div>
            <div class="distance-20-div"></div>
            <div class="clear"></div>
            <a class="open-signup white-a signup-a"><?php echo _JS_SIGNUP ?></a>
         </form>
  </div>

</div>

<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _JS_FORGOT_TITLE ?></h1>
    <form>
		<input class="clean  de-input" type="email" placeholder="<?php echo _JS_EMAIL ?>" required name="forgotPassword_email">

        <div class="clear"></div>
        <button class="clean blue-button mid-button-width small-distance small-distance-bottom">Submit</button>
        <div class="clear"></div>
        <a class="open-login white-a login-a"><?php echo _JS_LOGIN ?></a>
    </form>
  </div>

</div>


<!-- Login Modal -->
<div id="signup-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content signup-modal-content">
    <span class="close-css close-signup">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _JS_SIGNUP ?></h1>
         <form>
         	<div class="dual-input-div">
            	<p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
         		<input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" id="" name="" required>
            </div>
         	<div class="dual-input-div second-dual-input">
            	<p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
         		<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" id="" name="" required>
            </div>  
            <div class="clear"></div>          
         	<div class="dual-input-div">
            	<p class="input-top-text"><?php echo _JS_GENDER ?></p>
         		<select class="clean de-input" id="" name="" required>
                	<option><?php echo _JS_MALE ?></option>
                    <option><?php echo _JS_FEMALE ?></option>
                </select>
            </div>
         	<div class="dual-input-div second-dual-input">
            	<p class="input-top-text"><?php echo _JS_BIRTHDAY ?></p>
         		<input class="clean de-input p6" type="date" id="" name="" required>
            </div>  
            <div class="clear"></div>
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean de-input p6" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="" name="" required>
            <div class="clear"></div>
           	<div class="dual-input-div">
            	<p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
         		<select class="clean de-input" id="" name="" required>
                	<option><?php echo _JS_MALAYSIA ?></option>
                    <option><?php echo _JS_SINGAPORE ?></option>
                </select>
            </div>
         	<div class="dual-input-div second-dual-input">
            	<p class="input-top-text"><?php echo _JS_PHONE ?></p>
         		<input class="clean de-input p6" type="number" id="" name="" required>
            </div>            
            <div class="clear"></div> 
            <button class="clean blue-button mid-button-width small-distance-bottom" ><?php echo _JS_REQUEST_TAC ?></button>
            <div class="clear"></div>    
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="" name="" required>
            <div class="clear"></div> 
            <div class="dual-input-div">
            	<p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
				<input class="clean de-input" type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="password" name="password" required>
            </div>
         	<div class="dual-input-div second-dual-input">
            	<p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
         		<input class="clean de-input" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="password" name="password" required>
            </div>               
             <div class="clear"></div>
            <button class="clean blue-button mid-button-width small-distance small-distance-bottom" name="loginButton"><?php echo _JS_SIGNUP ?></button>
            <div class="clear"></div>
            <a class="open-login white-a login-a"><?php echo _JS_LOGIN ?></a>
         </form>
  </div>

</div>

<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
    
    
<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var loginmodal = document.getElementById("login-modal");
var signupmodal = document.getElementById("signup-modal");

var openforgot = document.getElementsByClassName("open-forgot")[0];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];

var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];

if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }   
}
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
      
<script>
function openTab(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" activetab", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " activetab";
}
</script>
	
		
		
		