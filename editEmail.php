<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/editEmail.php" />
    <meta property="og:title" content="Change Email | De Xin Guo Ji 德鑫国际" />
    <title>Change Email | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/editEmail.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _HEADER_CHANGE_EMAIL ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box smaller-box">
    	
         <form>
             <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean de-input p6" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="" name="" required>

             <div class="clear"></div>
            <button class="clean blue-button mid-button-width small-distance small-distance-bottom"><?php echo _JS_SUBMIT ?></button>
            <div class="clear"></div>
            
         </form>
	</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>