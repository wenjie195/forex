<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$totalProfit = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('LOSE'), "s");
$totalLose = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('WIN'), "s");

$tradeDetailsWin = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('LOSE'), "s");
$tradeDetailsLose = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('WIN'), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminTotalProfitLoss.php" />
    <meta property="og:title" content="Total Profit/Loss | De Xin Guo Ji 德鑫国际" />
    <title>Total Profit/Loss | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminTotalProfitLoss.php" />

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
	<div class="same-padding2 menu-distance">
		<h1 class="h1-title white-text text-center"><?php echo _SIDEBAR_TOTAL_PROFIT ?></h1>    
        <div  class="width100 overflow blue-opa-bg border-div small-distance tab-div">
            <div class="width100 overflow tab-divdiv tab">
                <button class="tablinks clean" onclick="openTab(event, 'Transaction')"><?php echo _ADMINDASHBOARD_NO_OF_WIN ?></button>
                <button class="tablinks clean" onclick="openTab(event, 'Position')"><?php echo _ADMINTOTALPROFITLOSS_TOTAL_PROFIT_USD ?></button>
                <button class="tablinks clean" onclick="openTab(event, 'Win')"><?php echo _ADMINDASHBOARD_NO_OF_LOSS ?></button>
                <button class="tablinks clean" onclick="openTab(event, 'Loss')"><?php echo _ADMINTOTALPROFITLOSS_TOTAL_LOSS_USD ?></button>
            </div>

            <div id="Transaction" class="tabcontent activetab small-distance block">
                <h3 class="text-center more-mbtm"><?php echo _ADMINDASHBOARD_NO_OF_WIN ?></h3>
                <table class="table-width data-table total-table">
                <thead>
                    <tr>
                        <th><?php echo _ADMINTOTALPROFITLOSS_DATE ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CUSTOMER_NAME ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($tradeDetailsWin != null)
                        {
                        for($cnt = 0;$cnt < count($tradeDetailsWin) ;$cnt++)
                        {?>
                        <tr>
                            <!-- <td><?php //echo $tradeDetailsWin[$cnt]->getDateCreated();?></td> -->
                            <td><?php echo date('d/m/Y', strtotime($tradeDetailsWin[$cnt]->getDateCreated()));?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getTimeline();?> <?php echo _USERDASHBOARD_SEC ?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getUsername();?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getCurrency();?></td>
                        </tr>
                        <?php
                        }
                        }
                        ?>
                    </tbody>
                </table>              
            </div>

            <div id="Position" class="tabcontent small-distance">
                <h3 class="text-center more-mbtm"><?php echo _ADMINTOTALPROFITLOSS_TOTAL_PROFIT_USD ?></h3>
                    <table class="table-width data-table total-table">
                    <thead>
                    <tr>
                        <th><?php echo _ADMINTOTALPROFITLOSS_DATE ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CUSTOMER_NAME ?></th>
                        <th><?php echo _USERDASHBOARD_AMOUNT ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- <tr>
                        <td>1/1/2020</td>
                        <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                        <td>Username</td>
                        <td>$ 50</td>
                        <td>USD/RMB</td>
                    </tr>
                    <tr>
                        <td>1/1/2020</td>
                        <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                        <td>Username</td>
                        <td>$ 50</td>
                        <td>USD/RMB</td>
                    </tr> -->
                    <?php
                    if($tradeDetailsWin != null)
                    {
                        for($cnt = 0;$cnt < count($tradeDetailsWin) ;$cnt++)
                        {?>
                        <tr>
                            <td><?php echo date('d/m/Y', strtotime($tradeDetailsWin[$cnt]->getDateCreated()));?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getTimeline();?> <?php echo _USERDASHBOARD_SEC ?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getUsername();?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getAmount();?></td>
                            <td><?php echo $tradeDetailsWin[$cnt]->getCurrency();?></td>
                        </tr>
                        <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>           
            </div>
    
        <div id="Win" class="tabcontent small-distance">
            <h3 class="text-center more-mbtm"><?php echo _ADMINDASHBOARD_NO_OF_LOSS ?></h3>
            <table class="table-width data-table total-table">
                <thead>
                    <tr>
                        <th><?php echo _ADMINTOTALPROFITLOSS_DATE ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CUSTOMER_NAME ?></th>
                        <th><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></th>
                    </tr>
                </thead>
                <tbody>
                    <!-- <tr>
                        <td>1/1/2020</td>
                        <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                        <td>Username</td>
                        <td>USD/RMB</td>
                    </tr>
                    <tr>
                        <td>1/1/2020</td>
                        <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                        <td>Username</td>
                        <td>USD/RMB</td>
                    </tr> -->
                    <?php
                    if($tradeDetailsLose != null)
                    {
                        for($cnt = 0;$cnt < count($tradeDetailsLose) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo date('d/m/Y', strtotime($tradeDetailsLose[$cnt]->getDateCreated()));?></td>
                            <td><?php echo $tradeDetailsLose[$cnt]->getTimeline();?> <?php echo _USERDASHBOARD_SEC ?></td>
                            <td><?php echo $tradeDetailsLose[$cnt]->getUsername();?></td>
                            <td><?php echo $tradeDetailsLose[$cnt]->getCurrency();?></td>
                        </tr>
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table> 
        </div>

        <div id="Loss" class="tabcontent small-distance">
            <h3 class="text-center more-mbtm"><?php echo _ADMINTOTALPROFITLOSS_TOTAL_LOSS_USD ?></h3>
            <table class="table-width data-table total-table">
                <thead>
                <tr>
                    <th><?php echo _ADMINTOTALPROFITLOSS_DATE ?></th>
                    <th><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></th>
                    <th><?php echo _ADMINTOTALPROFITLOSS_CUSTOMER_NAME ?></th>
                    <th><?php echo _USERDASHBOARD_AMOUNT ?></th>
                    <th><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></th>
                </tr>
                </thead>
                <tbody>
                <!-- <tr>
                    <td>1/1/2020</td>
                    <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                    <td>Username</td>
                    <td>$ 50</td>
                    <td>USD/RMB</td>
                </tr>
                <tr>
                    <td>1/1/2020</td>
                    <td>30 <?php //echo _USERDASHBOARD_SEC ?></td>
                    <td>Username</td>
                    <td>$ 50</td>
                    <td>USD/RMB</td>
                </tr> -->
                <?php
                if($tradeDetailsLose != null)
                {
                    for($cnt = 0;$cnt < count($tradeDetailsLose) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td><?php echo date('d/m/Y', strtotime($tradeDetailsLose[$cnt]->getDateCreated()));?></td>
                        <td><?php echo $tradeDetailsLose[$cnt]->getTimeline();?> <?php echo _USERDASHBOARD_SEC ?></td>
                        <td><?php echo $tradeDetailsLose[$cnt]->getUsername();?></td>
                        <td><?php echo $tradeDetailsLose[$cnt]->getAmount();?></td>
                        <td><?php echo $tradeDetailsLose[$cnt]->getCurrency();?></td>
                    </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>   
        </div>
    

    


        </div>
    </div>





</div>


<?php include 'js.php'; ?>


</body>
</html>