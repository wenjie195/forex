<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$withdrawalDetails = getWithdrawal($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminUserAddCredit.php" />
    <meta property="og:title" content="Top Up Credit | De Xin Guo Ji 德鑫国际" />
    <title>Top Up Credit | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminUserAddCredit.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<div class="dark-bg overflow same-padding">
    <?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <h1 class="menu-distance h1-title white-text text-center"><?php echo _ADMINMEMBER_ADDCREDIT ?></h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">
            <form action="utilities/submitTopUpFunction.php" method="POST">
                <div class="width100 shipping-div2">
                    <table class="withdraw-table">
                        <tbody>
                            <?php
                            // if(isset($_POST['user_uid']))
                            // {
                                $conn = connDB();
                                //Order
                                $depositArray = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
                                if($depositArray != null)
                                {
                                ?>
                                <tr>
                                    <td><?php echo _MAINJS_INDEX_USERNAME ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getUsername()?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _AUACCURRENTCREDIT_CCREDIT ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getCredit()?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _AUACCURRENTCREDIT_TUAMOUNT ?></td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" id="topup_amount" name="topup_amount" placeholder="<?php echo _AUACCURRENTCREDIT_TUAMOUNT ?>" class="clean de-input">
                                    </td>
                                </tr>
                                <?php
                                }else{
                                    echo "refresh";
                                }
                                $conn->close();
                           // }
                            ?>
                        </tbody>
                    </table>
                </div>
                
                <input type="hidden" id="user_uid" name="user_uid" value="<?php echo $depositArray[0]->getUid()?>">
                <input type="hidden" id="current_credit" name="current_credit" value="<?php echo $depositArray[0]->getCredit()?>">
                <input type="hidden" id="total_deposit" name="total_deposit" value="<?php echo $depositArray[0]->getDeposit()?>">

                <div class="clear"></div>

                <button class="clean hover1 blue-button smaller-font" type="submit" name="top_up" value="Submit">
                    <?php echo _AUACCURRENTCREDIT_SUBMIT ?>
                </button>
            </form>
        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>