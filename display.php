<?php
// set API Endpoint and API key
// $endpoint = 'latest';
// $access_key = 'ee34cf5a0d55806abbd902c8745af340';

// Initialize CURL:
// $ch = curl_init('http://data.fixer.io/api/'.$endpoint.'?access_key='.$access_key.'');
// $ch = curl_init('https://api.polygon.io/v1/last_quote/currencies/AUD/EUR?apiKey=CIS_tP7aDeaI0NQZTKnebo4J_NH7ld57R1ZMs4');
$ch = curl_init('https://api.polygon.io/v2/snapshot/locale/global/markets/forex/tickers?apiKey=CIS_tP7aDeaI0NQZTKnebo4J_NH7ld57R1ZMs4');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Store the data:
$json = curl_exec($ch);
curl_close($ch);

// Decode JSON response:
$exchangeRates = json_decode($json, true);

// print_r($exchangeRates['tickers']);
// Access the exchange rate values, e.g. GBP:

?>
  <?php
  if ($exchangeRates['tickers']) {
  for ($cnt=0; $cnt <count($exchangeRates['tickers']) ; $cnt++) {
    $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']);
    if ($selectCountry == 'MYRUSD') {
      ?>
      <tr>
        <td class="first-width"><?php  echo $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']); ?></td>
          <td><?php echo $exchangeRates['tickers'][$cnt]['lastQuote']['b']; ?></td>
        <td><?php   echo $exchangeRates['tickers'][$cnt]['lastQuote']['a']; ?></td>
        <?php if ($exchangeRates['tickers'][$cnt]['todaysChange'] < 0) {
          ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
        }elseif ($exchangeRates['tickers'][$cnt]['todaysChange'] >= 0) {
          ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
        } ?>
        <?php if ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] < 0) {
          ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
        }elseif ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] >= 0) {
            ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
        } ?>
        <td><button class="clean transparent-btn blue-button buy-btn-size open-sell"><?php echo _USERDASHBOARD_SELL ?></button></td>
        <td><button class="clean transparent-btn blue-button buy-btn-size open-buy"><?php echo _USERDASHBOARD_BUY ?></button></td>
      </tr> <?php
    }}
    for ($cnt=0; $cnt <count($exchangeRates['tickers']) ; $cnt++) {
      $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']);
      if ($selectCountry == 'USDMYR') {
        ?>
        <tr>
          <td class="first-width"><?php  echo $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']); ?></td>
            <td><?php echo $exchangeRates['tickers'][$cnt]['lastQuote']['b']; ?></td>
          <td><?php   echo $exchangeRates['tickers'][$cnt]['lastQuote']['a']; ?></td>
          <?php if ($exchangeRates['tickers'][$cnt]['todaysChange'] < 0) {
            ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
          }elseif ($exchangeRates['tickers'][$cnt]['todaysChange'] >= 0) {
            ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
          } ?>
          <?php if ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] < 0) {
            ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
          }elseif ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] >= 0) {
              ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
          } ?>
          <td><button class="clean transparent-btn blue-button buy-btn-size open-sell"><?php echo _USERDASHBOARD_SELL ?></button></td>
          <td><button class="clean transparent-btn blue-button buy-btn-size open-buy"><?php echo _USERDASHBOARD_BUY ?></button></td>
        </tr> <?php
      }}
      for ($cnt=0; $cnt <count($exchangeRates['tickers']) ; $cnt++) {
        $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']);
        if ($selectCountry == 'AUDUSD') {
          ?>
          <tr>
            <td class="first-width"><?php  echo $selectCountry = str_replace("C:","",$exchangeRates['tickers'][$cnt]['ticker']); ?></td>
              <td><?php echo $exchangeRates['tickers'][$cnt]['lastQuote']['b']; ?></td>
            <td><?php   echo $exchangeRates['tickers'][$cnt]['lastQuote']['a']; ?></td>
            <?php if ($exchangeRates['tickers'][$cnt]['todaysChange'] < 0) {
              ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
            }elseif ($exchangeRates['tickers'][$cnt]['todaysChange'] >= 0) {
              ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChange']; ?></td> <?php
            } ?>
            <?php if ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] < 0) {
              ?><td class="red-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
            }elseif ($exchangeRates['tickers'][$cnt]['todaysChangePerc'] >= 0) {
                ?><td class="green-text"><?php   echo $exchangeRates['tickers'][$cnt]['todaysChangePerc']; ?></td> <?php
            } ?>
            <td><button class="clean transparent-btn blue-button buy-btn-size open-sell"><?php echo _USERDASHBOARD_SELL ?></button></td>
            <td><button class="clean transparent-btn blue-button buy-btn-size open-buy"><?php echo _USERDASHBOARD_BUY ?></button></td>
          </tr> <?php
        }}}
  ?>
