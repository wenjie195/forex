<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitWithdrawal($conn,$uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status)
{
     if(insertDynamicData($conn,"withdrawal",array("uid","username","bank_name","bank_acc_number","amount","current_credit","status"),
     array($uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status),"sssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $uid = rewrite($_POST["withdrawal_uid"]);
    $name = rewrite($_POST["withdrawal_bank_acc_holder"]);
    $bankName = rewrite($_POST["withdrawal_bank_name"]);
    $bankAccNumber = rewrite($_POST["withdrawal_bank_acc_number"]);
    $amount = rewrite($_POST["withdrawal_amount"]);
    $currentCredit = rewrite($_POST["withdrawal_currentcredit"]);
    $status = "PENDING";

    $latestCredit = $currentCredit - $amount;

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $bankName."<br>";
     // echo $amount."<br>";
     // echo $currentCredit."<br>";
     // echo $status."<br>";
     // echo $latestCredit."<br>";

     if(submitWithdrawal($conn,$uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status))
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../admin1Product.php?type=4');
          // echo "submit withdrawal success";  

          if(isset($_POST['withdrawal_uid']))
          {   
              $tableName = array();
              $tableValue =  array();
              $stringType =  "";
              //echo "save to database";
              if($latestCredit)
              {
                  array_push($tableName,"credit");
                  array_push($tableValue,$latestCredit);
                  $stringType .=  "s";
              }              
              array_push($tableValue,$uid);
              $stringType .=  "s";
              $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
              
              if($orderUpdated)
              {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../adminShipping.php?type=11');
                    echo "<script>alert('Submit Withdrawal Request Successfully !');window.location='../userDashboard.php'</script>";
              }
              else
              {
                    //echo "fail aa";
                    echo "<script>alert('Unable to deduct credit in user side !');window.location='../userDashboard.php'</script>";
              }
          }
          else
          {
               echo "<script>alert('ERROR !');window.location='../userDashboard.php'</script>";
          }


     }
     else
     {
          echo "<script>alert('Fail to Submit Withdrawal Request !');window.location='../userDashboard.php'</script>";
     }

}
else
{
     echo "<script>alert('Server Problem !');window.location='../index.php'</script>";
}
?>
