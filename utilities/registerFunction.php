<?php
// require_once dirname(__FILE__) . '/../adminAccess.php';
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$fullName,$nationality,$userType)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","full_name","nationality","user_type"),
//           array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$fullName,$nationality,$userType),"ssssssssi") === null)
//      {
//           echo "gg";
//           // header('Location: ../addReferee.php?promptError=1');
//           //     promptError("error registering new account.The account already exist");
//           //     return false;
//      }
//      else{    }
//      return true;
// }

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$userType)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","user_type"),
          array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$userType),"ssssssi") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerUpline($conn,$referralId,$referralName,$referrerId)
{
     if(insertDynamicData($conn,"referral_history",array("referral_id","referral_name","referrer_id"),
          array($referralId,$referralName,$referrerId),"sss") === null)
     {
          echo "unable to register";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $register_firstname = rewrite($_POST['register_firstname']);
     // $register_lastname = rewrite($_POST['register_lastname']);
     // $nbsp = " ";
     // $fullName = $register_firstname.$nbsp.$register_lastname;

     // $register_gender = rewrite($_POST['register_gender']);
     $email = rewrite($_POST['register_email_user']);
     // $nationality = rewrite($_POST['register_country']);
     $phoneNo = rewrite($_POST['register_phone']);
     $username = rewrite($_POST['register_username']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $userType = "1";

     $referralName = rewrite($_POST['register_referral_name']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $fullName."<br>";
     // echo $nationality."<br>";
     // echo $userType."<br>";

     // echo $salt."<br>";
     // echo $finalPassword."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
               $userEmailDetails = $userEmailRows[0];

               $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
               $userPhoneDetails = $userPhoneRows[0];

               // if (!$userEmailDetails && !$userPhoneDetails)
               if (!$userEmailDetails && !$userPhoneDetails && !$usernameDetails)
               {
                    // if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$fullName,$nationality,$userType))
                    if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$userType))
                    {
                         // echo "done register";
                         // echo "<script>alert('Register Success !');window.location='../index.php'</script>";    

                         if($referralName != "")
                         {

                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($referralName),"s");
                              $uplineName = $usernameRows[0]->getUid();
                              $referrerId = $uplineName;

                              $referralId = $uid;
                              $referralName = $username;

                              if(registerUpline($conn,$referralId,$referralName,$referrerId))
                              {
                                   // echo "register with upline success";
                                   // echo "<script>alert('Register With Upline Success !');window.location='../index.php'</script>";    

                                   $_SESSION['uid'] = $referralId;
                                   $_SESSION['usertype_level'] = 1;
                                   echo "<script>alert('Register Success With Upline !');window.location='../userDashboard.php'</script>";
                                   // header('Location: ../userDashboard.php');
                              } 
                              else
                              { 
                                   // echo "unable to register upline";
                                   echo "<script>alert('Unable to find referral name !');window.location='../index.php'</script>";
                              }
                         }
                         else
                         { 
                              // echo "no upline during register";
                              // echo "<script>alert('Register Success Without Upline!');window.location='../index.php'</script>";

                              $_SESSION['uid'] = $uid;
                              $_SESSION['usertype_level'] = 1;
                              echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";

                         }

                    }
               }
               else
               {
                    //echo "fail to register register";
                    echo "<script>alert('register details has been used by others');window.location='../index.php'</script>";
               }
          }
          else 
          {
               // echo "password must be more than 6";
               echo "<script>alert('password must be more than 6');window.location='../index.php'</script>";
          }
     }
     else 
     {
          // echo "password and retype password not the same";
          echo "<script>alert('password and retype password not the same');window.location='../index.php'</script>";
     }      
}
else 
{
     header('Location: ../addReferee.php');
}

?>