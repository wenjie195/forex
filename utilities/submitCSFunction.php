<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$adminStatus)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","admin_status"),
     array($uid,$message_uid,$receiveSMS,$adminStatus),"ssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $message_uid = md5(uniqid());
     $uid = rewrite($_POST["sender_uid"]);
     $receiveSMS = rewrite($_POST["message_details"]);
     $adminStatus = "GET";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $message_uid."<br>";
     // echo $uid."<br>";
     // echo $receiveSMS."<br>";


     if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$adminStatus))
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../admin1Product.php?type=4');
          echo "<script>alert('message sent');window.location='../userDashboard.php'</script>"; 
          // echo "message sent";  
     }
     else
     {
          // echo "fail to sent message";
          echo "<script>alert('fail to sent message');window.location='../userDashboard.php'</script>"; 
     }

}
else
{
     header('Location: ../submitDeposit.php');
}
?>
