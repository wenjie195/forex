<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$totalProfit = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('LOSE'), "s");
$totalLose = getBetstatus($conn, "WHERE result_edited = ? ", array("result_edited"), array('WIN'), "s");
$withdrawalRequest = getWithdrawal($conn," WHERE status = ? ",array("status"),array('PENDING'),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Total Profit/Loss | De Xin Guo Ji 德鑫国际" />
    <title>Total Profit/Loss | De Xin Guo Ji 德鑫国际</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
	<div class="same-padding2 menu-distance">
		<h1 class="h1-title white-text text-center"><?php echo _SIDEBAR_TOTAL_PROFIT ?></h1>    
    	<div class="width100 overflow">
        	<a>
                <div class="four-div1 blue-border-hover">
                    <img src="img/win.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_NO_OF_WIN ?>" title="<?php echo _ADMINDASHBOARD_NO_OF_WIN ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_NO_OF_WIN ?></p> 
                        <?php
                        if($totalProfit)
                        {   
                            $totalProfitNumCount = count($totalProfit);
                        }
                        else
                        {   $totalProfitNumCount = 0;   }
                        ?>
                    
                </div>
            </a>

            <a href="adminReportWin.php" class="overflow">
                <div class="four-div1 blue-border-hover left-four-div1">
                    <img src="img/total-profit.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_TOTAL_WIN ?>" title="<?php echo _ADMINDASHBOARD_TOTAL_WIN ?>">
                        <p class="blue-border-p"><?php echo _ADMINDASHBOARD_TOTAL_WIN ?></p> 
                            <?php
                            if($totalProfit)
                            {
                                $totalProfitAmount = 0;
                                for ($cnt=0; $cnt <count($totalProfit) ; $cnt++)
                                {
                                $totalProfitAmount += $totalProfit[$cnt]->getAmount();
                                }
                            }
                            else
                            {
                                $totalProfitAmount = 0 ;
                            }
                            ?>
                        
                </div>
            </a>
        	<a>
                <div class="four-div1 blue-border-hover">
                    <img src="img/loss.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_NO_OF_LOSS ?>" title="<?php echo _ADMINDASHBOARD_NO_OF_LOSS ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_NO_OF_LOSS ?></p> 
                        <?php
                        if($totalLose)
                        {   
                            $totalProfitNumCount = count($totalLose);
                        }
                        else
                        {   $totalProfitNumCount = 0;   }
                        ?>
                         
                </div>
            </a> 
            <a href="adminReportLose.php" class="overflow">
                <div class="four-div1 blue-border-hover right-four-div1">
                    <img src="img/total-loss.png" class="blue-border-img" alt="<?php echo _ADMINDASHBOARD_TOTAL_LOSS ?>" title="<?php echo _ADMINDASHBOARD_TOTAL_LOSS ?>">
                    <p class="blue-border-p"><?php echo _ADMINDASHBOARD_TOTAL_LOSS ?></p> 
                            <?php
                            if($totalLose)
                            {
                                $totalLoseAmount = 0;
                                for ($cnt=0; $cnt <count($totalLose) ; $cnt++)
                                {
                                $totalLoseAmount += $totalLose[$cnt]->getAmount();
                                }
                            }
                            else
                            {
                                $totalLoseAmount = 0 ;
                            }
                            ?>
                   
                </div>
            </a>             
                                  
        </div>
    </div>





</div>


<?php include 'js.php'; ?>

</body>
</html>